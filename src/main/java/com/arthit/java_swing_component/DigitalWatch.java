/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import java.text.SimpleDateFormat;
import javax.swing.*;
import java.util.*;

/**
 *
 * @author Arthit
 */
public class DigitalWatch implements Runnable {

    JFrame frame;
    Thread thread = null;
    int hours = 0, minutes = 0, seconds = 0;
    String timeString = "";
    JButton button;

    DigitalWatch() {
        frame = new JFrame();

        thread = new Thread(this);
        thread.start();

        button = new JButton();
        button.setBounds(100, 100, 100, 50);

        frame.add(button);
        frame.setSize(300, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    public void run() {
        try {
            while (true) {

                Calendar calendar = Calendar.getInstance();
                hours = calendar.get(Calendar.HOUR_OF_DAY);
                System.out.println(hours);
                if (hours > 12) {
                    hours -= 12;
                }
                minutes = calendar.get(Calendar.MINUTE);
                seconds = calendar.get(Calendar.SECOND);

                SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss");
                Date date = calendar.getTime();
                timeString = formatter.format(date);

                printTime();

                thread.sleep(1000);
            }
        } catch (Exception e) {
        }
    }

    public void printTime() {
        button.setText(timeString);
    }

    public static void main(String[] args) {
        new DigitalWatch();

    }

}
