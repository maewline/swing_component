/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author Arthit
 */
public class JDesktopPane1 extends JFrame {

    public JDesktopPane1() {        
        CustomDesktopPane desktopPane = new CustomDesktopPane();        
        Container contentPane = getContentPane();        
        contentPane.add(desktopPane, BorderLayout.CENTER);        
        desktopPane.Display(desktopPane);        
        
        setTitle("JDesktopPane Example");        
        setSize(300, 350);        
        setVisible(true);        
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }    

    public static void main(String args[]) {        
        new JDesktopPane1();        
    }    
}

class CustomDesktopPane extends JDesktopPane {    

    int numFrames = 3, x = 30, y = 30;    

    public void Display(CustomDesktopPane display) {        
        for (int i = 0; i < numFrames; ++i) {            
            JInternalFrame jframe = new JInternalFrame("Internal Frame " + i, true, true, true, true);            
            
            jframe.setBounds(x, y, 250, 85);            
            Container container = jframe.getContentPane();            
            container.add(new JLabel("I love my country"));            
            display.add(jframe);            
            jframe.setVisible(true);            
            y += 85;            
        }        
    }    
}
