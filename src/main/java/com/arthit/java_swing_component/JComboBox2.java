/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Arthit
 */
public class JComboBox2 {

    JFrame frame;

    JComboBox2() {
        frame = new JFrame("Test ComboBox");
        JLabel label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setSize(400, 100);
        JButton button = new JButton("Show");
        button.setBounds(200, 100, 75, 20);
        String languages[] = {"C", "C++", "C#", "Java", "PHP"};
        JComboBox combo = new JComboBox(languages);
        combo.setBounds(50, 100, 90, 20);
        frame.add(combo);
        frame.add(label);
        frame.add(button);
        frame.setLayout(null);
        frame.setLocationRelativeTo(null);
        frame.setSize(350, 350);
        frame.setVisible(true);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String data = "Programing Language Selected: "
                        + combo.getItemAt(combo.getSelectedIndex());
                label.setText(data);
            }

        }
        );

    }

    public static void main(String[] args) {
        new JComboBox2();
    }
}
