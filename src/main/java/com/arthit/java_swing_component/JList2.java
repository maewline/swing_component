/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;

/**
 *
 * @author Arthit
 */
public class JList2 {

    JList2() {
        JFrame frame = new JFrame();
        final JLabel label = new JLabel();
        label.setSize(500, 100);
        JButton button = new JButton("Show");
        button.setBounds(200, 150, 80, 30);
        final DefaultListModel<String> DLM1 = new DefaultListModel<>();
        DLM1.addElement("C");
        DLM1.addElement("C++");
        DLM1.addElement("Java");
        DLM1.addElement("PHP");
        final JList<String> list1 = new JList<>(DLM1);
        list1.setBounds(100, 100, 75, 75);
        DefaultListModel<String> DLM2 = new DefaultListModel<>();
        DLM2.addElement("Turbo C++");
        DLM2.addElement("Struts");
        DLM2.addElement("Spring");
        DLM2.addElement("YII");
        final JList<String> list2 = new JList<>(DLM2);
        list2.setBounds(100, 200, 75, 75);
        frame.add(list1);
        frame.add(list2);
        frame.add(button);
        frame.add(label);
        frame.setSize(450, 450);
        frame.setLayout(null);
        frame.setVisible(true);
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String data = "";
                if (list1.getSelectedIndex() != -1) {
                    data = "Programming language Selected: " + list1.getSelectedValue();
                    label.setText(data);
                }
                if (list2.getSelectedIndex() != -1) {
                    data += ", FrameWork Selected: ";
                    for (Object frame : list2.getSelectedValues()) {
                        data += frame + " ";
                    }
                }
                label.setText(data);
            }
        });
    }

    public static void main(String args[]) {
        new JList2();
    }
}
