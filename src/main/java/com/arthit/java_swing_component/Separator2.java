/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSeparator;

/**
 *
 * @author Arthit
 */
public class Separator2 {
       public static void main(String args[]) {  
        JFrame frame = new JFrame("Separator Example");      
        frame.setLayout(new GridLayout(0, 1));  
        JSeparator separator3 = new JSeparator();
        separator3.setOpaque(true);
        separator3.setBackground(Color.red);
        frame.add(separator3);
        JLabel label1 = new JLabel("Above Separator");  
        frame.add(label1);  
        JSeparator separator = new JSeparator();  
        separator.setBackground(Color.blue);
        separator.setOpaque(true);
        frame.add(separator);  
        JSeparator separator2 = new JSeparator();
        JLabel label2 = new JLabel("Below Separator");  
        frame.add(label2);
        separator2.setBackground(Color.red);
        separator2.setOpaque(true);
        frame.add(separator2);
        frame.setSize(500, 500);  
        frame.setVisible(true);  
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
      }  
}
