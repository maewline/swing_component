/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import java.awt.Color;
import javax.swing.*;

/**
 *
 * @author Arthit
 */
public class JRootPane1 {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JRootPane root = frame.getRootPane();
        root.setBackground(Color.red);

        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("File");
        menuBar.add(menu);
        menu.add("Open");
        menu.add("Close");
        root.setJMenuBar(menuBar);
 
        root.getContentPane().add(new JButton("Press Me"));  
        frame.pack();
        frame.setVisible(true);
    }
}
