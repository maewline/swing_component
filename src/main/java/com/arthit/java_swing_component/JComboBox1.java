/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.JComboBox;
import javax.swing.JFrame;

/**
 *
 * @author Arthit
 */
public class JComboBox1 {

    JFrame frame;

    JComboBox1() {
        frame = new JFrame("ComboBox Example");
        String country[] = {"India", "Thailand", "England"};
        JComboBox comboBox = new JComboBox(country);
        comboBox.setBounds(50, 50, 90, 20);
        frame.add(comboBox);
        frame.setLocationRelativeTo(null);
        frame.setSize(400, 500);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new JComboBox1();
    }
}
