/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author Arthit
 */
public class ButtonExample1 {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Test Button");
        JButton b = new JButton("Click Here");
        b.setBounds(150, 110, 100, 30);
        frame.add(b);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
    }
}
