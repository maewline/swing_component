/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 *
 * @author Arthit
 */
public class JColorChooser2 extends JFrame implements ActionListener{  
JFrame frame;  
JButton button;  
JTextArea textArea;  
JColorChooser2(){  
    frame=new JFrame("Color Chooser Example.");  
    button=new JButton("Pad Color");  
    button.setBounds(200,250,100,30);  
    textArea=new JTextArea();  
    textArea.setBounds(10,10,300,200);  
    button.addActionListener(this);  
    frame.add(button);frame.add(textArea);  
    frame.setLayout(null);  
    frame.setSize(400,400);  
    frame.setVisible(true);  
}  
public void actionPerformed(ActionEvent e){  
    Color c=JColorChooser.showDialog(this,"Choose",Color.CYAN);  
    textArea.setBackground(c);  
}  
public static void main(String[] args) {  
    new JColorChooser2();  
}  
}     
