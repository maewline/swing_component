/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import javax.swing.*;

/**
 *
 * @author Arthit
 */
public class JScrollBar2 {

    JScrollBar2() {
        JFrame frame = new JFrame("Scrollbar Example");
        final JLabel label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setSize(400, 100);
        final JScrollBar scroll = new JScrollBar();
        scroll.setBounds(100, 100, 50, 100);
        frame.add(scroll);
        frame.add(label);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
        scroll.addAdjustmentListener(new AdjustmentListener() {
            public void adjustmentValueChanged(AdjustmentEvent e) {
                label.setText("Vertical Scrollbar value is:" + scroll.getValue());
            }
        });
    }

    public static void main(String args[]) {
        new JScrollBar2();
    }
}
