/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.JFrame;
import javax.swing.JScrollBar;



/**
 *
 * @author Arthit
 */
class JScrollBar1 {

    JScrollBar1() {
        JFrame frame = new JFrame("Scrollbar Example");
        JScrollBar scroll = new JScrollBar();
        scroll.setBounds(100, 100, 50, 100);
        frame.add(scroll);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    public static void main(String args[]) {
        new JScrollBar1();
    }
}
