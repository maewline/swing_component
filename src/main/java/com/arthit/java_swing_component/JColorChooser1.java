/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;

/**
 *
 * @author Arthit
 */
public class JColorChooser1 extends JFrame implements ActionListener {    
JButton button;    
Container container;    
JColorChooser1(){    
    container=getContentPane();    // this.getContentPane
    container.setLayout(new FlowLayout());         
    button=new JButton("color");    
    button.addActionListener(this);         
    container.add(button);    
}    
public void actionPerformed(ActionEvent e) {    
Color initialcolor=Color.RED;    
Color color=JColorChooser.showDialog(this,"Select a color",initialcolor);    
container.setBackground(color);    
}    
    
public static void main(String[] args) {    
    JColorChooser1 colorChooser=new JColorChooser1();    
    colorChooser.setSize(400,400);    
    colorChooser.setVisible(true);    
    colorChooser.setDefaultCloseOperation(EXIT_ON_CLOSE);   
    colorChooser.setLocationRelativeTo(null);
}    
}    
