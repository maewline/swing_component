/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Arthit
 */
public class JTable2 {    
      public static void main(String[] a) {  
            JFrame frame = new JFrame("Table Example"); 
            frame.setLocationRelativeTo(null);
            String data[][]={ {"101","Amit","670000"},    
                                     {"102","Jai","780000"},    
                                     {"101","Sachin","700000"}};    
            String column[]={"ID","NAME","SALARY"};         
            final JTable table=new JTable(data,column);    
            table.setCellSelectionEnabled(true);  
            ListSelectionModel select= table.getSelectionModel();  
            select.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);  
            select.addListSelectionListener(new ListSelectionListener() {  
              public void valueChanged(ListSelectionEvent e) {  
                String Data = null;  
                int[] row = table.getSelectedRows(); 
                int[] columns = table.getSelectedColumns();  
                for (int i = 0; i < row.length; i++) {  
                  for (int j = 0; j < columns.length; j++) {  
                    Data = (String) table.getValueAt(row[i], columns[j]); 
                  } }  
                System.out.println("Table element selected is: " + Data);    
              }       
            });  
            JScrollPane sp=new JScrollPane(table);    
            frame.add(sp);  
            frame.setSize(300, 200);  
            frame.setVisible(true);  
          }  
        }  
