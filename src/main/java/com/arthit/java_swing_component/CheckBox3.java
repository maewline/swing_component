/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author Arthit
 */
public class CheckBox3 extends JFrame implements ActionListener{  
    JLabel label;  
    JCheckBox checkbox1,checkbox2,checkbox3;  
    JButton button;  
    CheckBox3(){  
        label=new JLabel("Food Ordering System");  
        label.setBounds(50,50,300,20);  
        checkbox1=new JCheckBox("Pizza @ 100");  
        checkbox1.setBounds(100,100,150,20);  
        checkbox2=new JCheckBox("Burger @ 30");  
        checkbox2.setBounds(100,150,150,20);  
        checkbox3=new JCheckBox("Tea @ 10");  
        checkbox3.setBounds(100,200,150,20);  
        button=new JButton("Order");  
        button.setBounds(100,250,80,30);  
        button.addActionListener(this);  
        add(label);add(checkbox1);add(checkbox2);add(checkbox3);add(button);  
        setSize(400,400);  
        setLayout(null);  
        setVisible(true);  
        setDefaultCloseOperation(EXIT_ON_CLOSE);  
    }  
    public void actionPerformed(ActionEvent e){  
        float amount=0;  
        String msg="";  
        if(checkbox1.isSelected()){  
            amount+=100;  
            msg="Pizza: 100\n";  
        }  
        if(checkbox2.isSelected()){  
            amount+=30;  
            msg+="Burger: 30\n";  
        }  
        if(checkbox3.isSelected()){  
            amount+=10;  
            msg+="Tea: 10\n";  
        }  
        msg+="-----------------\n";  
        JOptionPane.showMessageDialog(this,msg+"Total: "+amount);  
    }  
    public static void main(String[] args) {  
        new CheckBox3();  
    }  
}  