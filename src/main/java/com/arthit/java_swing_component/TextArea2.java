/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

/**
 *
 * @author Arthit
 */


public class TextArea2 implements ActionListener {

    JLabel label1, label2;
    JTextArea area;
    JButton button;

    TextArea2() {
        JFrame frame = new JFrame();
        label1 = new JLabel();
        label1.setBounds(50, 25, 100, 30);
        label2 = new JLabel();
        label2.setBounds(160, 25, 100, 30);
        area = new JTextArea();
        area.setBounds(20, 75, 250, 200);
        button = new JButton("Count Words");
        button.setBounds(100, 300, 120, 30);
        button.addActionListener(this);
        frame.add(label1);
        frame.add(label2);
        frame.add(area);
        frame.add(button);
        frame.setSize(450, 450);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        String text = area.getText();
        String words[] = text.split(" ");
        label1.setText("Words: " + words.length);
        for(int i =0;i<words.length;i++){
            System.out.println(words[i]);
        }
        label2.setText("Characters: " + text.length());
    }

    public static void main(String[] args) {
        new TextArea2();
    }
}
