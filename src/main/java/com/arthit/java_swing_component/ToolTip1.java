/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.*;

/**
 *
 * @author Arthit
 */
public class ToolTip1 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Password Field Example");
        JPasswordField password = new JPasswordField();
        password.setBounds(100, 100, 100, 30);
        password.setToolTipText("Enter your Password");
        JLabel label = new JLabel("Password:");
        label.setToolTipText("Label");
        label.setBounds(20, 100, 80, 30);

        frame.add(password);
        frame.add(label);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
    }
}
