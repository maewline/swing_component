/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import java.awt.Color;
import javax.swing.*;

/**
 *
 * @author Arthit
 */
public class JEditorPane1 {

    JFrame frame = null;

    public static void main(String[] a) {
        (new JEditorPane1()).test();
    }

    private void test() {
        frame = new JFrame("JEditorPane Test");
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 200);
        JEditorPane editPane = new JEditorPane();
        editPane.setBackground(Color.red);
        editPane.setBounds(0, 0, 50, 50);
        editPane.setContentType("text/plain");
        editPane.setText("Sleeping is necessary for a healthy body."
                + " But sleeping in unnecessary times may spoil our health, wealth and studies."
                + " Doctors advise that the sleeping at improper timings may lead for obesity during the students days.");
        frame.add(editPane);
        frame.setVisible(true);
    }
}
