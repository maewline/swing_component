/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.*;
import java.awt.*;
import javax.swing.text.*;

/**
 *
 * @author Arthit
 */
public class JTextPane1 {

    public static void main(String args[]) throws BadLocationException {
        JFrame frame = new JFrame("JTextPane Example");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null); 
        Container container = frame.getContentPane();
        JTextPane textPane = new JTextPane();
        SimpleAttributeSet attributeSet = new SimpleAttributeSet();
        StyleConstants.setBold(attributeSet, true);

        textPane.setCharacterAttributes(attributeSet, true);
        textPane.setText("Welcome");

        attributeSet = new SimpleAttributeSet();
        StyleConstants.setItalic(attributeSet, true);
        StyleConstants.setForeground(attributeSet, Color.red);
        StyleConstants.setBackground(attributeSet, Color.blue);

        Document document = textPane.getStyledDocument();
        document.insertString(document.getLength(), "To Java ", attributeSet);

        attributeSet = new SimpleAttributeSet();
        document.insertString(document.getLength(), "World", attributeSet);

        JScrollPane scrollPane = new JScrollPane(textPane);
        container.add(scrollPane, BorderLayout.CENTER);

        frame.setSize(400, 300);
        frame.setVisible(true);
    }
}
