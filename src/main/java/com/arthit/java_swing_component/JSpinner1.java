/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.*;

/**
 *
 * @author Arthit
 */
public class JSpinner1 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Spinner Example");
        SpinnerModel spinModel = new SpinnerNumberModel(0, 0, 10, 1);
        JSpinner spinner = new JSpinner(spinModel);
        spinner.setBounds(100, 100, 50, 30);
        frame.add(spinner);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        
        
    }
}
