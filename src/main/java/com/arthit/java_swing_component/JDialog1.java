/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Arthit
 */
public class JDialog1 {

    private static JDialog dialog;

    JDialog1() {
        JFrame frame = new JFrame();
        dialog = new JDialog(frame, "Dialog Example", true);
        dialog.setLayout(new FlowLayout());
        dialog.setLocationRelativeTo(null);
        JButton button = new JButton("OK");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JDialog1.dialog.setVisible(false);
            }
        }
     );
        dialog.add(new JLabel("Click button to continue."));
        dialog.add(button);
        dialog.setSize(300, 300);
        dialog.setVisible(true);
    }

    public static void main(String args[]) {
        new JDialog1();
    }
}
