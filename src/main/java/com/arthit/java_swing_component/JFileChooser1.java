/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.*;
import java.awt.event.*;
import java.io.*;

/**
 *
 * @author Arthit
 */
public class JFileChooser1 extends JFrame implements ActionListener {

    JMenuBar menuBar;
    JMenu menu_File;
    JMenuItem menuItem_Openfile;
    JTextArea textArea;

    JFileChooser1() {
        menuItem_Openfile = new JMenuItem("Open File");
        menuItem_Openfile.addActionListener(this);
        menu_File = new JMenu("File");
        menu_File.add(menuItem_Openfile);
        menuBar = new JMenuBar();
        menuBar.setBounds(0, 0, 800, 20);
        menuBar.add(menu_File);
        textArea = new JTextArea(800, 800);
        textArea.setBounds(0, 20, 800, 800);
        add(menuBar);
        add(textArea);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == menuItem_Openfile) {
            JFileChooser fileChooser = new JFileChooser();
            int i = fileChooser.showOpenDialog(this);
            System.out.println(i);
            if (i == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                String filepath = file.getPath();
                try {
                    BufferedReader bufferReader = new BufferedReader(new FileReader(filepath));
                    String string1 = "", string2 = "";
                    while ((string1 = bufferReader.readLine()) != null) {
                        string2 += string1 + "\n";
                    }
                    textArea.setText(string2);
                    bufferReader.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        JFileChooser1 test = new JFileChooser1();
        test.setSize(500, 500);
        test.setLayout(null);
        test.setVisible(true);
        test.setLocationRelativeTo(null);
        test.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

}
