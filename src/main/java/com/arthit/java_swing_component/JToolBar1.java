/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author Arthit
 */
public class JToolBar1 {
    
    public static void main(final String args[]) {        
        JFrame frame = new JFrame("JToolBar Example");        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        
        JToolBar toolbar = new JToolBar();        
        toolbar.setRollover(true);        
        JButton button = new JButton("File");        
        toolbar.add(button);        
        toolbar.addSeparator();        
        toolbar.add(new JButton("Edit"));        
        toolbar.add(new JComboBox(new String[]{"Opt-1", "Opt-2", "Opt-3", "Opt-4"}));        
        Container contentPane = frame.getContentPane();        
        contentPane.add(toolbar, BorderLayout.NORTH);        
        JTextArea textArea = new JTextArea();        
        JScrollPane scrollPane = new JScrollPane(textArea);        
        contentPane.add(scrollPane, BorderLayout.EAST);        
        frame.setSize(450, 250);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);        
    }    
    
}
