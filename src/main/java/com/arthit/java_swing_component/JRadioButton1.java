/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JRadioButton;

/**
 *
 * @author Arthit
 */
public class JRadioButton1 {
    JFrame frame;
    JRadioButton1(){
        frame = new JFrame("Test");
        JRadioButton radioButton1 = new JRadioButton("Male");
        JRadioButton radioButton2 = new JRadioButton("Female");
        radioButton1.setBounds(75, 50, 100, 30);
        radioButton2.setBounds(75, 100, 100, 30);
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(radioButton1); buttonGroup.add(radioButton2);
        frame.setSize(300, 300);
        frame.add(radioButton1);
        frame.add(radioButton2);
        frame.setLayout(null);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
    public static void main(String[] args) {
        new JRadioButton1();
    }
}
