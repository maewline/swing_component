/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author Arthit
 */
public class PasswordField2 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Test JPasswordField");
        JLabel label = new JLabel();
        label.setBounds(20, 150, 200, 50);
        JPasswordField password = new JPasswordField();
        password.setBounds(100, 75, 100, 30);
        JLabel usernameLB = new JLabel("Username:");
        usernameLB.setBounds(20, 20, 80, 30);
        JLabel passwordLB = new JLabel("Password:");
        passwordLB.setBounds(20, 75, 80, 30);
        JButton button = new JButton("Login");
        button.setBounds(100, 120, 80, 30);
        JTextField field = new JTextField();
        field.setBounds(100, 20, 100, 30);
        frame.add(label);
        frame.add(password);
        frame.add(usernameLB);
        frame.add(passwordLB);
        frame.add(button);
        frame.add(field);
        
        frame.setLocationRelativeTo(null);
        frame.setSize(500, 300);
        frame.setLayout(null);
        frame.setVisible(true);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String data = "Username: " + field.getText();
                data += "   Password: " + new String (password.getPassword());
                label.setText(data);
            }

        }
        );
    }
}
