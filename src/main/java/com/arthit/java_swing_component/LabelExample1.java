/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Arthit
 */
public class LabelExample1 {
    public static void main(String[] args) {
         JFrame frame = new JFrame("Label Example");
         frame.setSize(500, 500);
         frame.setLayout(null);
         JLabel label1,label2;
         label1 = new JLabel("One");
         label2 = new JLabel("Two");
         label1.setBounds(250, 10, 30, 20);
         label2.setBounds(250, 25, 30, 20);
         frame.add(label1);
         frame.add(label2);
         frame.setLocationRelativeTo(null);
         frame.setVisible(true);
    }
}
