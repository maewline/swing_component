/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author Arthit
 */
public class DisplayingImage extends Canvas {

    public void paint(Graphics g) {

        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Image image = toolkit.getImage("C:\\Users\\Arthit\\Downloads\\love.jfif");
        g.drawImage(image, 120, 100, this);

    }

    public static void main(String[] args) {
        DisplayingImage main = new DisplayingImage();
        JFrame frame = new JFrame();
        frame.add(main);
        frame.setLocationRelativeTo(null);
        frame.setSize(400, 400);
        frame.setVisible(true);
    }

}
