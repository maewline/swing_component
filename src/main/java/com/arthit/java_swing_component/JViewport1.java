/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.LineBorder;


/**
 *
 * @author Arthit
 */
public class JViewport1 {  
    public static void main(String[] args) {  
        JFrame frame = new JFrame("Tabbed Pane Sample");  
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        frame.setLocationRelativeTo(null);
  
        JLabel label = new JLabel("Label");  
        label.setBackground(Color.green);
        label.setOpaque(true);
        label.setPreferredSize(new Dimension(700, 700));  
        JScrollPane jScrollPane = new JScrollPane(label);  
  
        JButton jButton1 = new JButton();  
        jScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);  
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);  
        jScrollPane.setViewportBorder(new LineBorder(Color.yellow));  
        jScrollPane.getViewport().add(jButton1, null);  
  
        frame.add(jScrollPane, BorderLayout.CENTER);  
        frame.setSize(400, 150);  
        frame.setVisible(true);  
    }  

}
