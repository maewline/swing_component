/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 *
 * @author Arthit
 */
public class TextArea1 {

    TextArea1() {
        JFrame frame = new JFrame();
        JTextArea area = new JTextArea("Welcome to my home");
        area.setBounds(10, 30, 200, 200);
        frame.add(area);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

    public static void main(String[] args) {
        new TextArea1();
    }
}
