/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.JCheckBox;
import javax.swing.JFrame;

/**
 *
 * @author Arthit
 */
public class CheckBox1 {
    CheckBox1(){
        JFrame frame = new JFrame("Check Box Example");
        JCheckBox checkBox1 = new JCheckBox("EIEI",true);
        checkBox1.setBounds(100,100, 50, 50);
        JCheckBox checkBox2 = new JCheckBox("JOJO");
        checkBox2.setBounds(100, 150, 100, 50);
        frame.add(checkBox1);
        frame.add(checkBox2);
        frame.setSize(400,400);
        frame.setLocationRelativeTo(null);
        frame.setLayout(null);
        frame.setVisible(true);
        
    }
    public static void main(String[] args) {
        new CheckBox1();
    }
}
