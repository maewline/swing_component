/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Arthit
 */
public class CheckBox2 {

    CheckBox2() {
        JFrame frame = new JFrame("CheckBox Example");
        JLabel label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setSize(400, 100);
        JCheckBox checkbox1 = new JCheckBox("Extra");
        checkbox1.setBounds(150, 100, 100, 50);
        JCheckBox checkbox2 = new JCheckBox("Arthit");
        checkbox2.setBounds(150, 150, 100, 50);
        frame.add(label);
        frame.add(checkbox1);
        frame.add(checkbox2);
        checkbox1.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                label.setText("Extra Checkbox: " + (e.getStateChange() == 1 ? "checked" : "unchecked"));
            }

        }
        );
        checkbox2.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                label.setText("Arthit Checkbox:" + (e.getStateChange() == 1 ? "check" : "unchecked"));
            }

        }
        );

        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new CheckBox2();
    }
}
