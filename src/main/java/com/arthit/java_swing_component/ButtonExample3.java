/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;


/**
 *
 * @author Arthit
 */
public class ButtonExample3 {
    ButtonExample3(){
        JFrame frame = new JFrame("Button Example");
        JButton button = new JButton(new ImageIcon("C:\\Users\\Arthit\\Downloads\\Thuch.png"));
        button.setBounds(400, 310, 309, 206);
        frame.add(button);
        frame.setSize(1000,800);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        
    }
    public static void main(String[] args) {
        new ButtonExample3();
    }
}
