/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.*;

/**
 *
 * @author Arthit
 */
public class MenuExample1 {

    JMenu menu, subMenu, menu2;
    JMenuItem item1, item2, item3, item4, item5, sub1, sub2, sub3;

    MenuExample1() {
        JFrame frame = new JFrame("Menu and MenuItem Example");
        JMenuBar menuBar = new JMenuBar();
        JMenuBar menuBar2 = new JMenuBar();
        menu2 = new JMenu("Subject");
        sub1 = new JMenuItem("Eng");
        sub2 = new JMenuItem("Math");
        sub3 = new JMenuItem("Com");
        menu2.add(sub1);
        menu2.add(sub2);
        menu2.add(sub3);
        menuBar2.add(menu2);
        menuBar2.setLocation(50, 30);
        frame.add(menuBar2);
        frame.setJMenuBar(menuBar2);

//        menu = new JMenu("Menu");
//        subMenu = new JMenu("Sub Menu");
//        item1 = new JMenuItem("Item 1");
//        item2 = new JMenuItem("Item 2");
//        item3 = new JMenuItem("Item 3");
//        item4 = new JMenuItem("Item 4");
//        item5 = new JMenuItem("Item 5");
//        menu.add(item1);
//        menu.add(item2);
//        menu.add(item3);
//        subMenu.add(item4);
//        subMenu.add(item5);
//        menu.add(subMenu);
//        menuBar.add(menu);
//        frame.setJMenuBar(menuBar);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    public static void main(String args[]) {
        new MenuExample1();
    }

}
