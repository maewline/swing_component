/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

/**
 *
 * @author Arthit
 */
public class JRadioButton2 extends JFrame implements ActionListener{
    JRadioButton rb1,rb2;
    JButton b1;
    JRadioButton2(){
        rb1 = new JRadioButton("Male");
        rb1.setBounds(100, 50, 100, 30);
        rb2 = new JRadioButton("Female");
        rb2.setBounds(100, 100, 100, 30);
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(rb1);
        buttonGroup.add(rb2);
        b1 = new JButton("Click");
        b1.setBounds(100, 150, 80, 30);
        b1.addActionListener(this);
        this.add(rb1);
        this.add(rb2);
        this.add(b1);
        this.setSize(300, 300);
        this.setLocationRelativeTo(null);
        this.setLayout(null);
        this.setVisible(true);
        
        
        
    }
    @Override
    public void actionPerformed(ActionEvent e) {
       if(rb1.isSelected()){
           JOptionPane.showMessageDialog(this, "You are male");
       }if(rb2.isSelected()){
           JOptionPane.showMessageDialog(this, "Yor are Femal");
       }
    }
    public static void main(String[] args) {
        new JRadioButton2();
    }
}
