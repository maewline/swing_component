/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.JFrame;
import javax.swing.JProgressBar;

/**
 *
 * @author Arthit
 */
public class JProgressBar1 extends JFrame {

    JProgressBar progressBar;
    int i = 0, num = 0;

    JProgressBar1() {
        progressBar = new JProgressBar(0, 1000);
        progressBar.setBounds(40, 40, 160, 30);
        progressBar.setValue(0);
        progressBar.setStringPainted(true);
        this.add(progressBar);
        this.setSize(250, 150);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
    }

    public void iterate() {
        while (i <= 2000) {
            progressBar.setValue(i);
            i = i + 200;
            try {
                Thread.sleep(150);
            } catch (Exception e) {
            }
        }
    }

    public static void main(String[] args) {
        JProgressBar1 m = new JProgressBar1();
        m.setVisible(true);
        m.iterate();
    }
}
