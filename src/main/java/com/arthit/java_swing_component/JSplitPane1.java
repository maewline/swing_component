/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;
import javax.swing.*;
import java.awt.*;

/**
 *
 * @author Arthit
 */
public class JSplitPane1 {
    private static void createAndShow() {  
        final JFrame frame = new JFrame("JSplitPane Example");  
        frame.setLocationRelativeTo(null);
        frame.setSize(300, 300);  
        frame.setVisible(true);  
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  

        frame.getContentPane().setLayout(new FlowLayout());  
        String[] option1 = { "A","B","C","D","E" };  
        JComboBox comboBox1 = new JComboBox(option1);  
        String[] option2 = {"1","2","3","4","5"};  
        JComboBox comboBox2 = new JComboBox(option2);  
        Panel panel1 = new Panel();  
        panel1.add(comboBox1);  
        Panel panel2 = new Panel();  
        panel2.add(comboBox2);  
        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, panel1, panel2); 
        splitPane.setBackground(Color.red);
  
        frame.getContentPane().add(splitPane);  
    }  
    public static void main(String[] args) {  
        // Schedule a job for the event-dispatching thread:  
        // creating and showing this application's GUI.  
        javax.swing.SwingUtilities.invokeLater(new Runnable() {  
            public void run() {  
                createAndShow();  
            }  
        }
        );  
    }  
}
