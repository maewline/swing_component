/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;

/**
 *
 * @author Arthit
 */
public class PasswordField1 {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Test Password Field Example");
        JPasswordField password = new JPasswordField();
        JLabel label = new JLabel("Password");
        label.setBounds(20, 100, 80, 30);
        password.setBounds(100, 100, 100, 30);
        frame.add(label);
        frame.add(password);
        frame.setLayout(null);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 300);
    }
}
