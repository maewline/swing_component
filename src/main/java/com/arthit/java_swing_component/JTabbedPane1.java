/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

/**
 *
 * @author Arthit
 */
public class JTabbedPane1 {    

    JFrame frame;    

    JTabbedPane1() {        
        frame = new JFrame();        
        JTextArea textArea = new JTextArea(500, 400);        
        JPanel panel1 = new JPanel();        
        panel1.add(textArea);        
        JPanel panel2 = new JPanel();        
        JPanel panel3 = new JPanel();        
        JTabbedPane tabbedPane = new JTabbedPane();        
        tabbedPane.setBounds(50, 50, 700, 700);        
        tabbedPane.add("main", panel1);        
        tabbedPane.add("visit", panel2);        
        tabbedPane.add("help", panel3);        
        frame.add(tabbedPane);        
        frame.setSize(1000,1000);        
        frame.setLayout(null);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);        
    }    

    public static void main(String[] args) {        
        new JTabbedPane1();        
    }
}
