/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;

/**
 *
 * @author Arthit
 */
public class JList1 {
    JList1(){  
        JFrame frame= new JFrame();  
        DefaultListModel<String> list1 = new DefaultListModel<>();  
          list1.addElement("Item1");  
          list1.addElement("Item2");  
          list1.addElement("Item3");  
          list1.addElement("Item4");  
          JList<String> list = new JList<>(list1);  
          list.setBounds(100,100, 75,75);  
          frame.add(list);  
          frame.setSize(400,400);  
          frame.setLayout(null);  
          frame.setVisible(true);  
}
    public static void main(String[] args) {
        new JList1();
    }
}
