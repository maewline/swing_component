/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.JButton;
import javax.swing.JFrame;


/**
 *
 * @author Arthit
 */
public class IntroSwing3 extends JFrame{

    JFrame frame;

    public IntroSwing3() {
        frame = new JFrame();
        JButton button = new JButton("Click");
        button.setBounds(130, 100, 100, 40);
        frame.add(button);
        frame.setSize(400, 500);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
    }

    public static void main(String[] args) {
        new IntroSwing3();
    }
}
