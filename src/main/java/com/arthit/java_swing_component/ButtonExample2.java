/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author Arthit
 */
public class ButtonExample2 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Button Example");
        frame.setSize(300,300);
        JTextField textField = new JTextField();
        textField.setBounds(50, 50, 150, 20);
        JButton bt = new JButton("Click Here");
        bt.setBounds(50, 100, 95, 30);
        bt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textField.setText("Welcome to thailand");
            }

        }
        );

         frame.add(bt);
         frame.add(textField);
         frame.setLayout(null);
         frame.setVisible(true);
         frame.setLocationRelativeTo(null);
    }
}
