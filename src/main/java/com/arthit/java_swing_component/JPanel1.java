/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Arthit
 */
public class JPanel1 {

    JPanel1() {
        final JFrame frame = new JFrame("Panel Example");
        JPanel panel = new JPanel();
        panel.setBounds(40, 80, 200, 200);
        panel.setBackground(Color.gray);
        JButton button1 = new JButton("Button 1");
        button1.setBounds(50, 100, 80, 30);
        button1.setBackground(Color.yellow);
        JButton button2 = new JButton("Close");
        button2.setBounds(100, 100, 80, 30);
        button2.setBackground(Color.green);
        panel.add(button1);
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.setVisible(false);
                frame.dispose();
            }
        }
        );
        panel.add(button2);
        frame.add(panel);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static void main(String args[]) {
        new JPanel1();
    }
}
