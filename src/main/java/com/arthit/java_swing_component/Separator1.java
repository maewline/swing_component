/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.java_swing_component;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author Arthit
 */
public class Separator1 {

    JMenu menu, subMenu;
    JMenuItem item1, item2, item3, item4, item5;

    Separator1() {
        JFrame frame = new JFrame("Separator Example");
        JMenuBar menubar = new JMenuBar();
        menu = new JMenu("Menu");
        item1 = new JMenuItem("Item 1");
        item2 = new JMenuItem("Item 2");
        menu.add(item1);
        menu.addSeparator();
        menu.add(item2);
        menu.addSeparator();
        menubar.add(menu);
        item3 = new  JMenuItem("Item3");
        item4 = new JMenuItem("Item4");
        menu.add(item3);
        menu.add(item4);
        frame.setJMenuBar(menubar);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    public static void main(String args[]) {
        new Separator1();
    }
}
